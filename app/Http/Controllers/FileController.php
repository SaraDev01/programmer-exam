<?php

namespace App\Http\Controllers;

use App\File;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FileController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('files.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:jpeg,bmp,png,gif,svg,pdf|max:500', // 1MB
        ]);

        // Handel File Upload To avoid duplicated file names
        $fileNameWithExt = $request->file('file')->getClientOriginalName(); // file name with extension
        $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME); // file name only
        $fileExtension = $request->file('file')->getClientOriginalExtension(); // file extension only
        $fileNameToStore = $fileName . '_' . time() . '.' . $fileExtension; // rename the file

        $path = $request->file('file')->storeAs('public/files', $fileNameToStore);

        $attribute = [
            'name' => $fileNameToStore,
            'user_id' => auth()->id(),
        ];

        File::create($attribute);

        return redirect('/file');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function show(File $file)
    {
        return view('files.show', [
            'user' => $file->user,
            'file' => $file,
            'allUserFiles' => $file->user->files,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = auth()->user()->files;
        
        $forwards = DB::table('forwards')
            ->select('files.name', 'admins.email')
            ->join('files', 'forwards.file_id', '=', 'files.id')
            ->join('admins', 'admins.id', '=', 'forwards.admin_id')
            ->where('forwards.user_id', '=', auth()->id())
            ->get();

        return view('files.index', compact('files', 'forwards'));
    }

}
