<?php

namespace App\Http\Controllers;

use App\File;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display all files to the admin
     */
    public function index()
    {
        $files = File::all();
        return view('admin-home', compact('files'));
    }

    /**
     * Display the forward page
     */
    public function showForwardForm()
    {
        return view('admin.forward', [
            'users' => User::all('id', 'email'),
            'files' => File::all('id', 'name'),
        ]);
    }

    /**
     * Insert the forward info into database
     */
    public function forward(Request $request)
    {
        $request->validate([
            'user1' => 'required',
            'user2' => 'required|different:user1',
            'file' => 'required',
        ]);

        $data = [[
            'admin_id' => auth()->id(),
            'user_id' => $request->user1,
            'file_id' => $request->file,
            'created_at' => Carbon::now()->toDateTimeString(),
        ], [
            'admin_id' => auth()->id(),
            'user_id' => $request->user2,
            'file_id' => $request->file,
            'created_at' => Carbon::now()->toDateTimeString(),
        ]];

        DB::table('forwards')->insert($data);

        return redirect('/admin/home');
    }

}
