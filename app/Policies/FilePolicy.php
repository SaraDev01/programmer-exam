<?php

namespace App\Policies;

use App\User;
use App\File;
use Illuminate\Auth\Access\HandlesAuthorization;

class FilePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the = file.
     *
     * @param  \App\User  $user
     * @param  \App\=File  $=File
     * @return mixed
     */
    public function view(User $user, File $file)
    {
        return $user->id == $file->user_id;
    }
}
