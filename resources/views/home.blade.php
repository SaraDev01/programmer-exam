@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">{{$user->name}} Dashboard</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card-body  row justify-content-center">
                    <a href="\file\create" class="btn btn-block btn-primary "> Go to upload file page </a>
                </div>
                <div class="card-body  row justify-content-center">
                    <a href="\file" class="btn btn-block btn-outline-primary"> View my files </a>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
