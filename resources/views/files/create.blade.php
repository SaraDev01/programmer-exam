@extends('layouts.app')
@section('content')
<br>
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-8">
         <div class="card">
            
            <div class="card-body">
            
                <form method="POST" action="/file" class="form-group" enctype="multipart/form-data">
                  @csrf
                  <h1 class="font-weight-bold">Upload New File</h1>
                  <br>
                  <div class="control-group">
                     <div class="control-group">
                        <input type="file" class="form-control-file" id="file" name="file" required/>
                     </div>
                  </div>
                  <br>
                  <button class="btn btn-primary btn-block" type="submit">Upload</button>
               </form>
            
            </div>
         </div>
      </div>
   </div>
</div>
@endsection


