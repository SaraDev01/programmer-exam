@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Your Files</div>

            <div class="card-body">
                <h1 class="font-weight-bold">Uploaded:</h1>
                @if ( $files->count())
                    <ul class="container list-group">
                        @foreach ($files as $file)
                            <li class="list-group-item">
                                {{ $file->name }}
                            </li>        
                        @endforeach
                    </ul>
                @else
                    <label>No Uploaded Files</label>
                @endif

                <br>
                <br>

                <h1 class="font-weight-bold">Forwarded: </h1>
                @if ( $files->count() )
                    <ul class="container list-group">
                        @foreach ($forwards as $forward)
                            <li class="list-group-item ">
                                {{ $forward->name }}<br>
                                &nbsp;&nbsp; <small>From: {{ $forward->email}}</small>
                            </li>        
                        @endforeach
                    </ul>
                @else
                    <label>No Forwarded Files</label>
                @endif
                
                <br>
                <br>
                <a href="\file\create" class="btn btn-primary btn-block"> Upload New File </a>

            </div>
        </div>
    </div>
</div>

@endsection