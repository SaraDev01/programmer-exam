@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
   <div class="col-md-8">
      <div class="card">
         <div class="card-body">

            <h1 class="font-weight-bold">Details</h1>
            <br>
            <br>

            <table class="table table-hover">
               <tbody>
                  <tr>
                     <td><label>Name</label></td>
                     <td>{{$user->name}}</td>
                  </tr>
                  <tr>
                     <td><label>Email</label></td>
                     <td>{{$user->email}}</td>
                  </tr>
                  <tr>
                     <td><label>File Name</label></td>
                     <td>{{$file->name}}</td>
                  </tr>
                  <tr>
                     <td><label>Uploaded At</label></td>
                     <td>{{$file->created_at}}</td>
                  </tr>
                  <tr>
                     <td><label>All User Files</label></td>
                     <td>
                        @if ( $allUserFiles->count())
                        <ul style="margin-left:-20px">
                           @foreach ($allUserFiles as $file)
                           <li>{{ $file->name }}
                              &nbsp;&nbsp; <small>{{$file->created_at}}</small>
                           </li>
                           @endforeach
                        </ul>
                        @else
                        <label>No Uploaded Files</label>
                        @endif
                     </td>
                  </tr>
               </tbody>
            </table>
            <br>
         </div>
      </div>
   </div>
</div>
@endsection


