@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Admin Dashboard</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                
                <br>
                <h1>All Files</h1>
                <br>

                {{-- Post list --}}
                @if ( count($files) > 0 )

                <table class="table table-hover">
                    <tbody>
                        @foreach ($files as $file)
                            <tr>
                                <td>{{ $file->name }}</td>
                                <td>
                                    <a href="/admin/file/{{$file->id}}" class="btn btn-outline-info btn-sm">Details</a>
                                </td>
                            </tr>    
                        @endforeach    
                    </tbody>
                </table>

                @else
                    <div class="text-center">
                        <h5>The are no files!</h5>
                    </div>
                @endif

                <br>
                {{-- Create Post List --}}
                <a href="/admin/forward" class="btn btn-primary btn-block">Forward</a>

                
            </div>
        </div>
    </div>
</div>
@endsection
