@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
   <div class="col-md-8">
      <div class="card">
         <div class="card-header">Forward</div>
         <div class="card-body">
            <h1 class="font-weight-bold">Details</h1>
            <br>
            <br>

            {{-- Forward Form --}}
            <form method="POST" action="/admin/forward" class="form-group" enctype="multipart/form-data">
               @csrf
               <table class="table table-hover">
                  <tbody>
                     <tr>
                        <td><label>User 1:</label></td>
                        <td>
                           <div class="form-group">
                              <select class="form-control" name="user1" required>
                                 <option value="" selected>Please Select the First Recipient</option>
                                 @foreach ($users as $user)
                                 <option value="{{$user->id}}">{{$user->email}}</option>
                                 @endforeach
                              </select>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td><label>User 2:</label></td>
                        <td>
                           <div class="form-group">
                              <select class="form-control" name="user2" required>
                                 <option value="" selected>Please Select the Second Recipient</option>
                                 @foreach ($users as $user)
                                 <option value="{{$user->id}}">{{$user->email}}</option>
                                 @endforeach
                              </select>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td><label>File:</label></td>
                        <td>
                           <div class="form-group">
                              <select class="form-control" name="file" id="file" required>
                                 <option value="{{ old('file') }}" selected>Please Select the File</option>
                                 @foreach ($files as $file)
                                 <option value="{{$file->id}}"  {{ (old('file') == $file->id)? "selected":"" }}>{{$file->name}}</option>
                                 @endforeach
                              </select>
                           </div>
                        </td>
                     </tr>
                  </tbody>
               </table>

               <button class="btn btn-primary btn-block" type="submit">Forward</button>
            </form>
         
        </div>
      </div>
   </div>
</div>
@endsection