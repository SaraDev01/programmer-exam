@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body  row justify-content-center">
                    <a class="btn btn-primary btn-block" href="\login">Login as User</a>
                </div>
                <div class="card-body  row justify-content-center">
                        <a class="btn btn-outline-primary btn-block" href="\admin\login">Login as Admin</a>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
