<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/home', 'AdminController@index')->name('admin.home');
    Route::get('/forward', 'AdminController@showForwardForm')->name('admin.forward');
    Route::post('/forward', 'AdminController@forward')->name('admin.forward.submit');
    Route::get('/file/{file}', 'FileController@show')->name('admin.file')->middleware('auth:admin');
});

Route::resource('file', 'FileController', [
    'only' => ['create', 'store', 'index']
])->middleware('auth');   


